/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#define rxDataLen 15
#include "string.h" 
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t rx_deal = 0; // 接收数据处理
uint8_t Run_deal = 0; // 是否启动
uint64_t count_tim8 = 0;
uint64_t count_tim12 = 0;
uint64_t set_freq = 100000; //设置的频率
uint64_t set_blink = 500; //设置的持续时间
char rxBuffer[rxDataLen]; //接收数组
uint64_t door_tim8 = 500*0.75  ; //定时器比较的门槛
//
uint64_t door_blink = 30; //相移的门槛
uint8_t once_time = 0; //只用执行一次
uint32_t ccr_ch4[2] = {0,25};//无用
uint8_t tim12_start_flag = 0;//12是否开始
uint16_t dead_time_set = 100; //死区时间设置 100 对应 620ns左右 200对应1.890us左右 越大死区时间越大但是容易造成不出波形 225 3.2us 205 2.06us 20就非常小了 170ns
//最低33kHZ 因为psc溢出了 最大只能是256 arr才是65536的位数

TIM_HandleTypeDef TIM1_Handler;         //定时器1PWM句柄 
TIM_OC_InitTypeDef TIM1_CH1Handler;	
void TIM_SetTIM1Compare1(uint32_t compare) //设置占空比
{
	TIM1->CCR1=compare; 
}

void claculate_fhz(uint64_t freq,uint16_t dead_time_temp)//修改定时器1的频率和死区函数
{
	float temp_psc = 168000000 / 10 / 2 /freq; //记得除以2
	uint8_t think = ((uint8_t)temp_psc*10) % 10;
	if(think > 5) temp_psc = (uint8_t)temp_psc+1;
	else temp_psc = (uint8_t)temp_psc;
	MX_TIM1_Init(temp_psc-1,9,dead_time_temp);
}

void claculate_door_tim8(uint64_t blink)//计算输出的时间
{
	if(blink < 300)
	{
		door_tim8 = (uint64_t)(blink * 0.70);
		door_tim8 = door_tim8 -10;
	}
	if(blink >= 300 && blink <=500)
	{
		door_tim8 = (uint64_t)(blink * 0.75);
	}
	if(blink > 500 && blink <=2000)
	{
		door_tim8 = (uint64_t)(blink * 0.8);
	}
	if(blink > 2000 && blink <= 4000)
	{
		door_tim8 = (uint64_t)(blink * 0.825);
	}
	if(blink > 4000)
	{
		door_tim8 = (uint64_t)(blink * 0.85);
	}
}

void claculate_door_blink()//计算相移时间，已经没有用处了
{
	float door_blink_temp = 3000000 / set_freq;
	uint8_t think = ((uint8_t)door_blink_temp*10) % 10;
	if(think > 5) door_blink = (uint8_t)door_blink_temp+1;
	else door_blink = (uint8_t)door_blink_temp;
}

void claculate_tim8(uint16_t psc_temp)//计算TIM8的时间也没有用处
{
	uint16_t psc_real= psc_temp / 10 * 168;
	MX_TIM8_Init(psc_real-1,9);
}

void rx_deal_inf()
{
	if((uint8_t)rxBuffer[0] != 0xCF || (uint8_t)rxBuffer[rxDataLen-1] != 0xFC)
        rx_deal=0;
	if(rx_deal == 1)
	{
		if((uint8_t)rxBuffer[1] == 0x01)
		{
			//char txBuffer[] = 
			//HAL_UART_Transmit(&huart1, (uint8_t *)txBuffer, strlen(txBuffer), 1000);
						set_freq = 1000 * ((uint16_t)rxBuffer[3] << 8 | (uint16_t)rxBuffer[2]); // 修改频率
            set_blink = (uint16_t)rxBuffer[7] << 8 | (uint16_t)rxBuffer[6]; // 修改闪烁时间
						dead_time_set = (uint16_t)rxBuffer[11] << 8 | (uint16_t)rxBuffer[10];//修改死区时间
						claculate_fhz(set_freq,dead_time_set); //作用于定时器直接修改频率和死区
			      //claculate_tim8(set_blink); //修改定时器TIM8
						claculate_door_tim8(set_blink); //作用于门槛，用于出波时间控制
						//claculate_door_blink();  //相移
            char txBuffer[] =("t7.txt=\"success\"\xff\xff\xff");
            HAL_UART_Transmit(&huart1, (uint8_t *)txBuffer, strlen(txBuffer), 1000);
            HAL_Delay(1000);//强制延时1s，这期间也不允许启动其他任务
            char txBuffer_none[] =("t7.txt=\"none\"\xff\xff\xff");
		        HAL_UART_Transmit(&huart1, (uint8_t *)txBuffer_none, strlen(txBuffer_none), 1000);
			      rxBuffer[0] = 0x00;//避免重复执行
		}
           
        else if((uint8_t)rxBuffer[1] == 0x02)
				{
					 char txBuffer_on[]=("t8.txt=\"On\"\xff\xff\xff");
					 HAL_UART_Transmit(&huart1, (uint8_t *)txBuffer_on, strlen(txBuffer_on), 1000);
           Run_deal = 1;//启动
					 once_time = 1;
					 rxBuffer[0] = 0x00;//避免重复执行

				}
        else if((uint8_t)rxBuffer[1] == 0x03)
				{
					 Run_deal = 0;
					 HAL_TIM_Base_Stop_IT(&htim8);//关闭时钟,使得不再计数
					 HAL_TIM_Base_Stop_IT(&htim12);
					 tim12_start_flag = 0;
					//可以重复执行
				}
      
        rx_deal = 0;
	}
}

void Run_machine()
{
	if(Run_deal == 1)
	{
		Run_deal = 2;//不再执行这条语句
		//重新初始化一下互补时钟
		claculate_fhz(set_freq,dead_time_set); 
		//打开四通道输出
		HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);
	  HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);
		//打开四通道输出
		HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_2);
	  HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_2);
		//开启TIM8定时器
		HAL_TIM_Base_Start_IT(&htim8);
		//允许tim12时钟运行
		tim12_start_flag = 1;
	}
	if(Run_deal == 0)
	{
		GPIO_InitTypeDef GPIO_InitStruct = {0};
		GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    //GPIO_InitStruct.Alternate = GPIO_AF1_TIM1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    //GPIO_InitStruct.Alternate = GPIO_AF1_TIM1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_11;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    //GPIO_InitStruct.Alternate = GPIO_AF1_TIM1;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
				
		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOB,GPIO_PIN_0,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOE,GPIO_PIN_11,GPIO_PIN_RESET);
		if(tim12_start_flag == 1)
		HAL_TIM_Base_Start_IT(&htim12);
	}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init(83,9,100);
  MX_USART1_UART_Init();
  MX_TIM8_Init(0,149);
  MX_TIM12_Init();
  /* USER CODE BEGIN 2 */
	
	//开启定时器
	
	//HAL_TIM_Base_Start_IT(&htim8);
	//HAL_TIM_Base_Stop_IT(&htim8);
	
	
	//TIM_SetTIM1Compare1(5);
	//MX_TIM1_Init(4,100);
	
	
	//TIM_SetTIM1Compare1(25);
	//HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);
	//HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);
	//HAL_Delay(1000);
	//HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_1);
	//HAL_TIMEx_PWMN_Stop(&htim1, TIM_CHANNEL_1);
	//HAL_Delay(1000);
	HAL_UART_Receive_IT(&huart1, (uint8_t *)&rxBuffer, rxDataLen); //开启接收中断
	//HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);
	//HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);
	//HAL_TIM_OC_Start_DMA(&htim1,TIM_CHANNEL_2, ccr_ch4,2);
	//HAL_TIMEx_OCN_Start_DMA(&htim1,TIM_CHANNEL_2, ccr_ch4,2);
	//char end_char[] =  {0xFF,0xFF,0xFF};
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		//HAL_UART_Transmit(&huart1, (uint8_t *)txBuffer, strlen(txBuffer), 1000);
		//HAL_UART_Transmit(&huart1, (uint8_t *)end_char, strlen(end_char), 1000);
    //HAL_Delay(1000);
		rx_deal_inf();
		Run_machine();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)//定时器回调函数
{
	if(htim == &htim8)
	{
		count_tim8++;//累加
	}
	if(htim == &htim12)
	{
		count_tim12++;//累加
	}
	if( count_tim8 >= door_tim8 / 2)
	{
			Run_deal = 0;
			HAL_TIM_Base_Stop_IT(&htim8);//关闭时钟
			count_tim8 = 0;
	}
	if(count_tim12 >= door_tim8)
	{
		//这三行注释掉就是原来的功能
		  Run_deal = 1;
		  HAL_TIM_Base_Stop_IT(&htim12);
		  count_tim12 = 0;
	}
	//if( count_tim8 >= door_blink && once_time == 1)
	
		//HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);
	  //HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);
		//once_time = 0;
	
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) 
{
    if(huart == &huart1) // 如果是串口1
    {
			rx_deal = 1;
      //HAL_UART_Transmit_IT(&huart1, (uint8_t *)&rxBuffer, rxDataLen);
    }

    // 使用HAL_UART_Receive_IT()时，只会进入一次中断。因此需要在回调函数内再次调用该函数
    HAL_UART_Receive_IT(&huart1, (uint8_t *)rxBuffer, rxDataLen);
}



/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
